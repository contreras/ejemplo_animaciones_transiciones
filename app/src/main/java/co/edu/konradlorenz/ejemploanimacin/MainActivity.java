package co.edu.konradlorenz.ejemploanimacin;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Transition transition;
    public static final long DURACION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Slide slideBottom = new Slide(Gravity.BOTTOM);
        slideBottom.setDuration(DURACION);
        slideBottom.setInterpolator(new DecelerateInterpolator());

        getWindow().setAllowReturnTransitionOverlap(false); //Evitar que las transiciones se envien a la vez
        getWindow().setReenterTransition(slideBottom);

        Button explodeButton = (Button) findViewById(R.id.button_explode);
        Button slideButton = (Button) findViewById(R.id.button_slide);
        Button fadeButton = (Button) findViewById(R.id.button_fade);

        Button normalButton = (Button) findViewById(R.id.button_normal);

        explodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transition = new Explode();
                iniciarActividad();
            }
        });

        slideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transition = new Slide(Gravity.LEFT);
                iniciarActividad();
            }
        });

        fadeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transition = new Fade(Fade.IN);
                iniciarActividad();

            }
        });

        normalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniciarActividad();

            }
        });
    }

    private void iniciarActividad(){
        transition.setDuration(DURACION);
        transition.setInterpolator(new DecelerateInterpolator());
        getWindow().setExitTransition(transition);
        Intent intent = new Intent(this,SecondActivity.class);
        startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
    }
}
