package co.edu.konradlorenz.ejemploanimacin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Button backButton = (Button) findViewById(R.id.button_back);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAfterTransition();
            }
        });

        Fade fadeIn = new Fade(Fade.IN);
        fadeIn.setDuration(MainActivity.DURACION);
        fadeIn.setInterpolator(new DecelerateInterpolator());

        Slide slideTop = new Slide(Gravity.TOP);
        slideTop.setDuration(MainActivity.DURACION);
        slideTop.setInterpolator(new DecelerateInterpolator());

        getWindow().setReturnTransition(slideTop);

        getWindow().setEnterTransition(fadeIn);

        getWindow().setAllowEnterTransitionOverlap(false);

    }

}
